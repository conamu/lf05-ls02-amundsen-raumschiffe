package com.raumschiffe;

/**
 * @author Constantin Amundsen
 * @version 1.0
 */

public class LogbuchEintrag {

    private String titel;
    private String inhalt;

    public LogbuchEintrag(String titel, String inhalt) {
        this.titel = titel;
        this.inhalt = inhalt;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getInhalt() {
        return inhalt;
    }

    public void setInhalt(String inhalt) {
        this.inhalt = inhalt;
    }
}