package com.raumschiffe;

/**
 * @author Constantin Amundsen
 * @version 1.0
 */

public class Ladung {

    private String name;
    private int menge;

    public Ladung (String name, int menge) {
        this.name = name;
        this.menge = menge;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }
}
