package com.raumschiffe;

import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * @author Constantin Amundsen
 * @version 1.0
 */

public class Main {

    /**
     * Vordefinierter Spielablauf
     * @param args args
     */
    public static void main(String[] args) {

        // Modul Setup
        ArrayList<Modul> klingonenModule = new ArrayList<Modul>();
        klingonenModule.add(new Modul("energieversorgung", 100));
        klingonenModule.add(new Modul("schild", 100));
        klingonenModule.add(new Modul("huelle", 100));
        klingonenModule.add(new Modul("lebenserhaltung", 100));

        ArrayList<Modul> romulanerModule = new ArrayList<Modul>();
        romulanerModule.add(new Modul("energieversorgung", 100));
        romulanerModule.add(new Modul("schild", 100));
        romulanerModule.add(new Modul("huelle", 100));
        romulanerModule.add(new Modul("lebenserhaltung", 100));

        ArrayList<Modul> vulkanierModule = new ArrayList<Modul>();
        vulkanierModule.add(new Modul("energieversorgung", 80));
        vulkanierModule.add(new Modul("schild", 80));
        vulkanierModule.add(new Modul("huelle", 50));
        vulkanierModule.add(new Modul("lebenserhaltung", 100));

        // Kapitän Setup
        Kapitaen klingonenKapitaen = new Kapitaen("Bumbum", 2206);
        Kapitaen romulanerKapitaen = new Kapitaen("Romulo", 2364);
        Kapitaen vulkanierKapitaen = new Kapitaen("Borg-Königin", 1778);

        // Raumschiff Setup
        Raumschiff klingonen = new Raumschiff(klingonenModule, klingonenKapitaen, "IKS Hegh'ta", 2);
        Raumschiff romulaner = new Raumschiff(romulanerModule, romulanerKapitaen, "IRW Khazara", 2);
        Raumschiff vulkanier = new Raumschiff(vulkanierModule, vulkanierKapitaen, "Ni'Var", 5);

        // Vorbereiten der Raumschiffe
        klingonen.beladen(new Ladung("Ferengi Schneckensaft", 200));
        klingonen.beladen(new Ladung("Bat'leth Klingonen Schwert", 200));
        klingonen.setTorpedos(1);

        romulaner.beladen(new Ladung("Borg-Schrott", 5));
        romulaner.beladen(new Ladung("Plasma-Waffe", 50));
        romulaner.setTorpedos(2);

        vulkanier.beladen(new Ladung("Forschungssonde", 35));
        vulkanier.beladen(new Ladung("photontorpedo", 3));

        // Kampf abfolge
        klingonen.torpedosAbschiessen(romulaner);
        romulaner.phaserFeuern(klingonen);
        vulkanier.broadcast("Gewalt ist nicht Logisch!");
        klingonen.zustand();
        klingonen.ladungAufraeumen();
        vulkanier.reparaturAuftrag(IntStream.of(0, 1, 2).toArray(), 5);
        vulkanier.torpedosLaden();
        klingonen.torpedosAbschiessen(romulaner);
        klingonen.zustand();
        klingonen.ladungsVerzeichnis();
        romulaner.zustand();
        romulaner.ladungsVerzeichnis();
        vulkanier.zustand();
        vulkanier.ladungsVerzeichnis();

    }
}
