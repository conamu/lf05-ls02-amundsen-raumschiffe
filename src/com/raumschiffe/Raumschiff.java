package com.raumschiffe;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Constantin Amundsen
 * @version 1.0
 */
public class Raumschiff {

    private String name;
    private int anzahlAndroiden;
    private List<Modul> module = new ArrayList<Modul>();
    private Kapitaen kapitaen;
    private List<LogbuchEintrag> logbuch = new ArrayList<LogbuchEintrag>();
    private String broadcastCommunicator = "";
    private List<Ladung> beladung = new ArrayList<Ladung>();
    private int torpedos;

    // Constructor
    public Raumschiff(List<Modul> module, Kapitaen kapitaen, String name, int anzahlAndroiden) {
        this.module = module;
        this.kapitaen = kapitaen;
        this.name = name;
        this.anzahlAndroiden = anzahlAndroiden;
    }

    public int getModuleByFunction(String function) {
        int moduleLocation = -1;
        for(int i = 0; i < module.size(); i++) {
            if(module.get(i).getName() == function) {
                moduleLocation = i;
            }
        }
        return moduleLocation;
    }

    public int getLadungByName(String name) {
        int location = -1;
        for (int i = 0; i < beladung.size(); i++) {
            if (beladung.get(i).getName() == name) {
                location = i;
            }
        }
        return location;
    }

    // Operative Funktionen

    /**
     *
     * @param ziel Ziel Raumschiff fuer Angriff
     *
     * Wenn torpedos an bord, wird die schadensmethode Aufgerufen.
     */
    public void torpedosAbschiessen(Raumschiff ziel) {
        if (torpedos <= 0) {
            broadcastCommunicator = "";
            logbuch.add(new LogbuchEintrag("Fehlermeldung", broadcastCommunicator));
            return;
        }

        torpedos -= 1;
        broadcastCommunicator = "Photontorpedo abgeschossen!";
        logbuch.add(new LogbuchEintrag("Bewaffnung", broadcastCommunicator));
        ziel.schaden();

    }

    /**
     * Wenn die Energieversorung ueber 50 ist, wird die Schadensmethode des ziels Aufgerufen.
     * @param ziel Ziel Raumschiff für den Angriff
     * @see Raumschiff Raumschiff
     */
    public void phaserFeuern(Raumschiff ziel) {

        // Finden des energiemoduls
        int energyModule = getModuleByFunction("energieversorgung");

        var em = module.get(energyModule);

        if (em.getZustand() < 50) {
            broadcastCommunicator = "-=*Click*=-";
            logbuch.add(new LogbuchEintrag("Fehler", broadcastCommunicator));
            return;
        }

        em.setZustand(em.getZustand() - 50);

        broadcastCommunicator = "Phaser wurden abgefeuert!";
        logbuch.add(new LogbuchEintrag("Bewaffnung", broadcastCommunicator));
        ziel.schaden();

    }

    /**
     * Findet die betroffenen Module mit getModuleByFunction()
     *
     * Folgende Logik wird verwendet, um die schadensberechnung durchzuführen:
     * Schild unter 50 = Beschädige die Huelle
     * Huelle kleiner/gleich 0 = Lebensversorgung zerstört.
     *
     * Wenn die Lebensversorgung zerstoert ist, wird ein Logbucheintrag generiert.
     *
     * @see #getModuleByFunction(String) getModuleByFunction()
     *
     **/
    protected void schaden() {
        broadcastCommunicator = this.name + " wurde getroffen!";
        logbuch.add(new LogbuchEintrag("Treffer", broadcastCommunicator));

        int schild = getModuleByFunction("schild");
        int huelle = getModuleByFunction("huelle");
        int lebenserhaltung = getModuleByFunction("lebenserhaltung");
        int energieversorgung = getModuleByFunction("energieversorgung");

        if (module.get(schild).getZustand() >= 50) {
            module.get(schild).setZustand(module.get(schild).getZustand() - 50);
            return;
        }

        if (module.get(huelle).getZustand() >= 50) {
            module.get(huelle).setZustand(module.get(huelle).getZustand() - 50);
            module.get(energieversorgung).setZustand(module.get(energieversorgung).getZustand() - 50);
            return;
        }

        if (module.get(huelle).getZustand() <= 0) {
            module.get(lebenserhaltung).setZustand(0);
            broadcastCommunicator = "Lebenserhaltung von " + this.name +  " wurde zerstört!";
            logbuch.add(new LogbuchEintrag("Modul", broadcastCommunicator));
            return;
        }
    }

    public void beladen(Ladung ladung) {
        beladung.add(ladung);
    }

    public List<LogbuchEintrag> logbuchEintraege() {
        return this.logbuch;
    }

    /**
     * Laedt bis zu 6 Torpedos in die rohre nach.
     * Sollten nicht genug Torpedos vorhanden sein, werden nur die vorhandenen Nachgeladen.
     *
     * Am Ende der operation wird ausgegeben, wieviele Torpedos nachgeladen wurden und das
     * Ladungsverzeichnis wird aufgeraeumt.
     */
    public void torpedosLaden() {
        int nachzuladendeTorpedos = 6;
        int ladungLocation = getLadungByName("photontorpedo");

        int torpedosImLaderaum = beladung.get(ladungLocation).getMenge();

        if (torpedosImLaderaum <= 0) {
            System.out.println("Keine Photontorpedos gefunden!");
            broadcastCommunicator = "-=*Click*=-";
        }

        if (torpedosImLaderaum < nachzuladendeTorpedos && torpedosImLaderaum > 0) {
            nachzuladendeTorpedos = torpedosImLaderaum;
            beladung.get(ladungLocation).setMenge(0);
        }

        beladung.get(ladungLocation).setMenge(beladung.get(ladungLocation).getMenge() - nachzuladendeTorpedos);

        torpedos = nachzuladendeTorpedos;

        System.out.println(nachzuladendeTorpedos + " Photontorpedos wurden nachgeladen!");

        ladungAufraeumen();

    }

    public void broadcast(String message) {
        this.broadcastCommunicator = message;
    }

    /**
     * Berechnet mithilfe einer Zufallszahl den Wert der Heilung für die Module.
     * @param modul Nimmt ein Array von Schiffsmodulen.
     * @param anzahlRoboter Menge der Roboter, welche das Schiff reparieren.
     * @see Modul modul-klasse
     */
    public void reparaturAuftrag(int[] modul, int anzahlRoboter) {
        Random rand = new Random();
        double n = 1.0;
        double modifier = rand.nextDouble() % n;

        if (anzahlRoboter > anzahlAndroiden) {
            anzahlRoboter = anzahlAndroiden;
        }

        double intmod = modifier * anzahlRoboter / module.size();

        for (int i = 0; i < modul.length; i++) {
            var m = module.get(modul[i]);
            var mZustand = (m.getZustand());

            m.setZustand(mZustand + (intmod*100));
        }
    }

    /**
     * Gibt die Ladungsliste aus, ruft davor ladungAufraeumen() auf.
     * @see #ladungAufraeumen() ladungAufraeumen()
     */
    public void ladungsVerzeichnis() {
        System.out.print("----------------------------------------------\n");
        System.out.println("Folgende Ladung ist an Bord:");

        for(int i = 0; i < beladung.size(); i++) {
            System.out.println(beladung.get(i).getName() + " | " + beladung.get(i).getMenge());
        }
        System.out.print("\n----------------------------------------------\n");

    }

    /**
     * Gibt relevante Informationen ueber das Raumschiff aus.
     */
    public void zustand() {
        System.out.print("----------------------------------------------\n");
        System.out.printf("Zustand des Raumschiffes: %s\n", this.name);
        System.out.printf("%s %.0f\n", module.get(0).getName(), module.get(0).getZustand());
        System.out.printf("%s %.0f\n", module.get(1).getName(), module.get(1).getZustand());
        System.out.printf("%s %.0f\n", module.get(2).getName(), module.get(2).getZustand());
        System.out.printf("%s %.0f\n", module.get(3).getName(), module.get(3).getZustand());
        System.out.printf("Es sind %d androiden an Bord.\n", anzahlAndroiden);
        System.out.printf("Kapitean ist %s seit %d\n", kapitaen.getName(), kapitaen.getKapitaenSeit());
        System.out.printf("Es sind %d Torpedos an Bord.\n", torpedos);
        System.out.print("----------------------------------------------\n");
    }

    public void ladungAufraeumen() {
        for (int i = 0; i < beladung.size(); i++) {
           if (beladung.get(i).getMenge() <= 0) {
               beladung.remove(i);
           }
        }
    }



    // Getter und Setter


    public List<Modul> getModule() {
        return module;
    }

    public void setModule(List<Modul> module) {
        this.module = module;
    }

    public Kapitaen getKapitaen() {
        return kapitaen;
    }

    public void setKapitaen(Kapitaen kapitaen) {
        this.kapitaen = kapitaen;
    }

    public List<LogbuchEintrag> getLogbuch() {
        return logbuch;
    }

    public void setLogbuch(List<LogbuchEintrag> logbuch) {
        this.logbuch = logbuch;
    }

    public String getBroadcastCommunicator() {
        return broadcastCommunicator;
    }

    public void setBroadcastCommunicator(String broadcastCommunicator) {
        this.broadcastCommunicator = broadcastCommunicator;
    }

    public List<Ladung> getBeladung() {
        return beladung;
    }

    public void setBeladung(List<Ladung> beladung) {
        this.beladung = beladung;
    }

    public int getTorpedos() {
        return torpedos;
    }

    public void setTorpedos(int torpedos) {
        this.torpedos = torpedos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
