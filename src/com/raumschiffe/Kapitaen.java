package com.raumschiffe;

/**
 * @author Constantin Amundsen
 * @version 1.0
 */

public class Kapitaen {

    private String name;
    private int kapitaenSeit;

    public Kapitaen(String name, int kapitaenSeit) {
        this.name = name;
        this.kapitaenSeit = kapitaenSeit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getKapitaenSeit() {
        return kapitaenSeit;
    }

    public void setKapitaenSeit(int kapitaenSeit) {
        this.kapitaenSeit = kapitaenSeit;
    }
}
