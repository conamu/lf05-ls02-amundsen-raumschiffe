package com.raumschiffe;

/**
 * @author Constantin Amundsen
 * @version 1.0
 */

public class Modul {

    private String name;
    private double zustand;

    public Modul(String name, double zustand) {
        this.name = name;
        this.zustand = zustand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getZustand() {
        return zustand;
    }

    public void setZustand(double zustand) {
        this.zustand = zustand;
    }
}
